## Description

* Generate data file
```
perl -Ilib bin/generate.pl -c 2000000 > calls.log
```
* Process data file
```
perl -Ilib bin/collect.pl -f ./calls.log -v
```
