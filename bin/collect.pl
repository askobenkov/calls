#!/usr/bin/perl

use strict;
use warnings;
no warnings qw/uninitialized redefine/;

use Data::Dumper;
use File::Temp qw/tempfile/;
use English q/-no_match_vars/;
use Params::Validate q/:all/;

use IPChain::Cache;
use IPChain::CmdOpt;
use IPChain::Utils;

use constant OK         => 0;
use constant ERROR      => 1;
use constant CHUNK_SIZE => 20;

my $definition = {
    filepath => {
        ds =>
            'Входной файл с логом телефонных звонков',
        t  => 'str',
        op => 'f',
    },
    process => {
        ds => 'Количество процессов',
        t  => 'str',
        op => 'p',
        df => 10
    },
    verbose => {
        ds => 'Отладочная информация',
        t  => 'true',
        op => 'v'
    },
};

my $options = init($definition);

exit run();

sub run {

    my $block_ref   = _get_block_indexes();
    my $db_filepath = _get_temporary_db_filepath();

    my $time = time();

    my $rc = work(
        sub {
            my $worker_index = shift;

            my $item_ref = $block_ref->[ $worker_index - 1 ];
            if ( !$item_ref ) {

                debug("Process with PID: '$$' exiting....");
                return;
            }

            my ( $from, $to ) = @$item_ref;
            _prepare_calls( $db_filepath, $options->{filepath}, $from, $to,
                $options->{verbose} );
        }
    );

    return $rc if ( $rc != OK );

    my $db_ref
        = IPChain::Cache->new(
        { filepath => $db_filepath, verbose => $options->{verbose} } );
    die "Can't init temporary db" unless ($db_ref);

    $db_ref->create_index();

    my $call_ref = $db_ref->select_max_operator();
    return $call_ref->{rc} if ( $call_ref->{rc} != OK );

    printf "Max operators: %d, total: %s sec.\n", $call_ref->{max_operator},
        time() - $time;

    return OK;
}

sub work {
    my $subref = shift;

    my $worker_pids_ref = {};
    foreach my $index ( 1 .. $options->{process} ) {

        my $pid = fork();
        if ( !defined $pid ) {

            debug( "Fork failed: $OS_ERROR", 'error' );
            return ERROR;
        }
        elsif ($pid) {

            debug(
                sprintf( "Worker #%d started with PID '%d'", $index, $pid ),
                'info' );
            $worker_pids_ref->{$pid} = { index => $index };
        }
        else {

            my $ret_code = eval {
                $subref->($index);

                OK;
            };
            if ( my $error = $EVAL_ERROR ) {

                debug(
                    sprintf(
                        "Worker #%d died with error: '%s'",
                        $index, $error
                    )
                );
            }
            $ret_code = ERROR if ( !defined $ret_code );

            exit($ret_code);
        }
    }

    my $rc = OK;
    while ( scalar keys %$worker_pids_ref ) {

        my $child_pid = wait();
        if ( $child_pid > 0 ) {

            my $child_exit_code = $CHILD_ERROR;
            my $child_rc        = $child_exit_code >> 8;
            my $item_ref        = delete $worker_pids_ref->{$child_pid};
            debug(
                sprintf(
                    "Worker #%d with PID '%s' finished with code: '%s'",
                    $item_ref->{index}, $child_pid, $child_rc
                ),
                'info'
            );

            if ( $child_rc != OK ) {
                $rc = $child_rc;
            }
        }
    }

    return $rc;
}

sub init {
    my $definition = shift;

    my $options = {};
    cmd_options( $options, $definition );

    unless ( $options->{filepath} && -e $options->{filepath} ) {
        cmd_usage($definition);
    }
    unless ( $options->{process} =~ /^\d+$/ && $options->{process} <= 15 ) {
        cmd_usage($definition);
    }

    return $options;
}

sub debug {
    IPChain::Utils::debug(@_) if ( $options->{verbose} );
}

=head1 PRIVATE METHOD

=cut

sub _get_block_indexes {
    my $lines_count = IPChain::Utils::get_lines_count( $options->{filepath} );
    debug( sprintf( "Process count: %d", $options->{process} ) );
    debug( sprintf( "Lines count: %d",   $lines_count ) );

    my $block_ref
        = IPChain::Utils::get_block_indexes( $lines_count,
        $options->{process} );
    debug( Dumper($block_ref) );

    return $block_ref;
}

sub _get_temporary_db_filepath {
    my ( undef, $db_filepath ) = tempfile(
        'callsXXXXXXXX',
        TMPDIR => 1,
        UNLINK => 0,
        SUFFIX => '.db'
    );

    debug( sprintf( "Temporary db filepath: %s", $db_filepath ) );
    return $db_filepath;
}

sub _prepare_calls {
    my ( $db_filepath, $filepath, $from, $to, $verbose ) = @_;

    my $db_ref
        = IPChain::Cache->new(
        { filepath => $db_filepath, verbose => $verbose } );
    die "Can't init temporary db" unless ($db_ref);

    debug( sprintf("PID: $$, filepath: $filepath, index: [$from - $to]") );

    open my $fh, '<', $filepath or die "Can't open file '$filepath'...$!";

    my $PER_COUNT = 450;

    #FROM:2019-05-20 05:08 TO:2019-05-20 05:31
    my $rgx
        = qr/^(FROM:(\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2})\s+TO:(\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}))/;
    my ( $lines_ref, $cur ) = ( [], 0 );
    while ( my $line = <$fh> ) {

        chomp $line;
        if ( $cur >= $from && $cur <= $to ) {

            if ( $line =~ $rgx ) {

                my ( $from, $to )
                    = map { IPChain::Utils::parsedate($_) } ( $2, $3 );
                push @$lines_ref, { from => $from, to => $to };
            }
            else {

                debug( sprintf( "Wrong line #%d format...", $cur + 1 ) );
            }

            if ( scalar @$lines_ref >= $PER_COUNT ) {

                my $call_ref
                    = $db_ref->insert_calls( { data => $lines_ref } );
                return $call_ref if ( $call_ref->{rc} != OK );
                $lines_ref = [];
            }
        }

        ++$cur;
    }

    my $call_ref
        = scalar @$lines_ref
        ? $db_ref->insert_calls( { data => $lines_ref } )
        : { rc => OK };

    close $fh;
    return $call_ref;
}

1;
