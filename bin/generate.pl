#!/usr/bin/perl

use strict;
use warnings;

use POSIX qw /strftime/;
use IPChain::CmdOpt;

=head1 NAME

    bin/generator.pl

=head1 SYNOPSIS

    perl -Ilib bin/generate.pl --help
    perl -Ilib bin/generate.pl -c 1000000 > calls.log

=head1 DESCRIPTION

Генерация лога телефонных звонков

=cut

my $definition = {
    count => {
        ds => 'Количество записей',
        t  => 'str',
        op => 'c',
        df => 15
    },
};

my $options = init($definition);

exit run();

sub run {
    for ( 1 ... $options->{count} ) {
        my $avg = time - rand(1000000);
        my $fr = strftime( "%Y-%m-%d %H:%M", localtime( $avg - rand(1000) ) );
        my $to = strftime( "%Y-%m-%d %H:%M", localtime( $avg + rand(1000) ) );
        print("FROM:$fr TO:$to \n");
    }
}

sub init {
    my $definition = shift;

    my $options = {};
    cmd_options( $options, $definition );

    unless ( $options->{count} ) {
        cmd_usage($definition);
    }

    return $options;
}
