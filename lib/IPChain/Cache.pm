package IPChain::Cache;

use strict;
use warnings;
no warnings 'redefine';

use DBI;
use Data::Dumper;
use Params::Validate qw/:all/;
use English qw/-no_match_vars/;

use IPChain::Utils;

use constant OK    => 0;
use constant ERROR => 1;

sub new {
    my $this = shift;

    my $params_ref = eval {
        validate_with(
            params => \@_,
            spec   => {
                filepath => { type => SCALAR },
                verbose  => { type => SCALAR | UNDEF, optional => 1 },
            }
        );
    };
    if ( my $error = $EVAL_ERROR ) {

        IPChain::Utils::debug( $error, 'error' );
        return undef;
    }

    my $self = bless $params_ref, $this;

    eval {

        $self->connect();
        $self->debug( sprintf( "database filepath '%s'", $self->{filepath} ),
            'info' );
        $self->create_db();
    };
    if ( my $error = $EVAL_ERROR ) {

        $self->debug( "can't init object...$error", 'error' );
        return undef;
    }

    return $self;
}

sub DESTROY {
    my $self = shift;
    $self->dbh->disconnect() if ( $self->dbh );
}

sub insert_calls {
    my $self = shift;

    my $params_ref = eval {
        validate_with(
            params => \@_,
            spec   => { data => { type => ARRAYREF }, },
        );
    };

    if ( my $error = $EVAL_ERROR ) {

        $self->debug( "failed validate params: $error", 'error' );
        return { rc => OK };
    }

    die "Can't create transaction"
        unless ( $self->begin_exclusive_transaction() );

    eval {
        my $query = sprintf( 'INSERT INTO calls(begin, end) VALUES %s;',
            join( ',', ('(?,?)') x scalar @{ $params_ref->{data} } ) );
        my $sth = $self->dbh->prepare($query)
            or die "Couldn't prepare statement: " . $self->dbh->errstr;

        $sth->execute( map { @$_{qw/from to/} } @{ $params_ref->{data} } )
            or die "can't execute statement: $sth->errstr";
    };

    if ( my $error = $EVAL_ERROR ) {

        $self->dbh->rollback();
        $self->debug( "insert calls failed...$error", 'error' );
        return { rc => ERROR };
    }

    $self->dbh->commit();

    return { rc => OK };
}

sub create_index {
    my $self = shift;

    die "Can't create transaction"
        unless ( $self->begin_exclusive_transaction() );

    eval {

        $self->dbh->do(
            'CREATE INDEX IF NOT EXISTS calls_begin_idx on calls(begin);');
        $self->dbh->do(
            'CREATE INDEX IF NOT EXISTS calls_end_idx on calls(end);');
    };

    if ( my $error = $EVAL_ERROR ) {

        $self->dbh->rollback();
        $self->debug( "create index failed...$error", 'error' );
        return { rc => ERROR };
    }

    $self->dbh->commit();

    return { rc => OK };
}

sub select_max_operator {
    my $self = shift;

    my $call_ref = {};
    eval {

        my $query = <<'SQL';
SELECT max((
    SELECT count(*) AS cnt FROM calls AS c1 WHERE c1.begin BETWEEN c3.begin AND c3.end
        OR c1.end BETWEEN c3.begin AND c3.end
)) AS max_operator FROM calls AS c3
SQL
        my $sth = $self->dbh->prepare($query)
            or die "Couldn't prepare statement: " . $self->dbh->errstr;

        $call_ref = $self->dbh->selectrow_hashref($sth);
    };
    if ( my $error = $EVAL_ERROR ) {

        $self->debug( "can't fetch record..$error", 'error' );
        return { rc => ERROR };
    }

    return { rc => OK, max_operator => $call_ref->{max_operator} };
}

sub dbh {
    my ( $self, $name ) = @_;
    return $self->{dbh};
}

sub connect {
    my $self = shift;
    unless ( $self->dbh ) {
        $self->{dbh} = DBI->connect(
            sprintf( "dbi:SQLite:dbname=%s", $self->{filepath} ),
            "", "",
            {   AutoCommit => 0,
                RaiseError => 1,
            }
        ) or die "can't connect to database: $DBI::errstr";
    }
}

sub create_db {
    my ($self) = @_;

    $self->dbh->do(<<'SQL');
CREATE TABLE IF NOT EXISTS calls(
    id integer primary key autoincrement,
    begin integer,
    end integer
 );
SQL

    $self->dbh->commit();
}

sub begin_exclusive_transaction {
    my $self = shift;

    my $attempt = 5;
    do {
        --$attempt;
        eval { $self->dbh->do('BEGIN EXCLUSIVE TRANSACTION'); };
        if ( my $error = $EVAL_ERROR ) {

            $self->debug( "Can't begin transaction...$error", 'warn' );
            sleep 1;
        }
        else {

            $attempt = -1;
        }
    } while ( $attempt > 0 );

    return $attempt < 0 ? 1 : 0;
}

sub debug {
    my $self = shift;
    IPChain::Utils::debug(@_) if ( $self->{verbose} );
}

1;
