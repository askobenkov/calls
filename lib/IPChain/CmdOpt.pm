package IPChain::CmdOpt;

BEGIN {
    use base 'Exporter';
    our @EXPORT = qw/cmd_options cmd_usage/;
}

use Getopt::Long;

use strict;

#       Типы опций
#       true - флаг наличия, str - строка, num - число
our $types = { true => '!', str => '=s', num => '=i', str_arr => '=s@{1,}' };

=head1
Формат опций:
        длинный_ключ => {
                op      => короткий ключ, по умолчанию - первая буква
                ds      => описание опции
                t       => тип опции
                df      => значение по дефолту
                f       => формат
=cut

sub cmd_options {
    my ( $opts, $opt ) = @_;
    my %o = ( 'h|help' => sub { cmd_usage($opt) } );

    foreach my $key ( keys %$opt ) {
        $opts->{$key} = $opt->{$key}->{df};
        $o{ cmd_getshort( $opt, $key ) . '|' . $key
                . $types->{ $opt->{$key}->{t} } } = \$opts->{$key};
    }
    my $stat = GetOptions(%o);
    return $opts;
}

#       Определение названия короткой опции
sub cmd_getshort {
    my ( $opt, $key ) = @_;
    return $opt->{$key}->{op} || substr( $key, 0, 1 );
}

#       Вывод помощи
sub cmd_usage {
    my $o = shift;
    my $wide_format
        = "-%s, --%s\t%s\t\t(по умолчанию - %s; формат - %s)\n";
    my $format = "-%s, --%s\t%s\n";

    print "usage: perl $0 [ключи]\n-------\n";
    foreach my $k ( sort keys %$o ) {
        if ( $o->{$k}->{df} || $o->{$k}->{f} ) {
            printf( $wide_format,
                cmd_getshort( $o, $k ),
                $k,
                $o->{$k}->{ds},
                _make_default_help( $o->{$k} ),
                $o->{$k}->{f} || 'не определено' );
        }
        else {
            printf( $format, cmd_getshort( $o, $k ), $k, $o->{$k}->{ds} );
        }
    }
    printf( $format,
        "h", "help", "Показать эту справку и выйти" );
    exit;
}

=head2 _make_default_help

Формирование корректной подсказки если передали массив значений в качестве дефолного значения

=cut

sub _make_default_help {
    my $option = shift;
    if ( $option->{t} eq 'str_arr' ) {
        return
            ref $option->{df}
            ? join( ' ', @{ $option->{df} } )
            : $option->{df};
    }
    return $option->{df} || 'не определено';
}

1;
