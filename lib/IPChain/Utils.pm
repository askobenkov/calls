package IPChain::Utils;

use strict;
use warnings;

BEGIN {
    use base 'Exporter';
    our @EXPORT = qw/get_lines_count get_block_indexes debug mkdate parsedate/;
}

use POSIX qw/ceil/;
use Time::Local;

sub get_lines_count {
    my $filepath = shift;

    open my $fh, '<', $filepath or die "Can't open '$filepath'...$!";
    1 while <$fh>;
    my $count = $.;
    close $fh;

    return $count;
}

sub get_block_indexes {
    my ( $lines_count, $process_count ) = @_;

    my $items = [ 0 .. ( $lines_count ? $lines_count - 1 : 0 ) ];
    my $list_ref = [];
    return $list_ref unless ( scalar @$items );

    my $cur_index = 0;
    my $per_count = ceil( $lines_count / ( $process_count || 1 ) ) || 1;

    my $last_index = scalar @$items - 1;
    do {
        my $from_index = $cur_index;
        my $to_index   = $cur_index + $per_count - 1;
        $to_index = $last_index if ( $to_index > $last_index );
        push @$list_ref, [ $from_index, $to_index ];
        $cur_index = $cur_index + $per_count;
    } while ( $cur_index <= $last_index );

    return $list_ref;
}

sub debug {
    my ( $msg, $type ) = @_;
    $type ||= 'info';
    printf STDERR
        sprintf( "[%s] [%s] %s\n", scalar localtime, uc $type, $msg )
        if ($msg);
}

sub mkdate {
    my ( $year, $month, $day, $hour, $min, $sec ) = @_;
    my $retval;
    eval {
        $retval = Time::Local::timelocal( $sec, $min, $hour, $day, $month - 1,
            $year );
    };
    return int $retval;
}

sub parsedate {
    my $date = shift;

    if ( $date
        =~ /^([0-9]{4})(?:\.|\-)([0-9]{1,2})(?:\.|\-)([0-9]{1,2})(?:\s+|T)?([0-9]{1,2}):([0-9]{1,2})(:([0-9]{1,2}))?/
        )
    {
        return mkdate( $1, $2, $3, $4, $5, $6 );

        #   17:13:30 27.08.2012
    }
    elsif ( $date
        =~ /^([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})(?:\s+|T)?([0-9]{1,2})(?:\.|\-)([0-9]{1,2})(?:\.|\-)([0-9]{4})/
        )
    {
        return mkdate( $6, $5, $4, $1, $2, $3 );

        #   2014.01.21
    }
    elsif ( $date =~ /^([0-9]{4})(?:\.|\-)([0-9]{1,2})(?:\.|\-)([0-9]{1,2})/ )
    {
        return mkdate( $1, $2, $3, 0, 0, 0 );

        #   21.01.2014
    }
    elsif ( $date
        =~ /^([0-9]{1,2})(?:\.|\-|\/)([0-9]{1,2})(?:\.|\-|\/)([0-9]{4})/ )
    {
        return mkdate( $3, $2, $1, 0, 0, 0 );

        #   01.2014
    }
    elsif ( $date =~ /^([0-9]{1,2})(?:\.|\-)([0-9]{4})$/ ) {
        return mkdate( $2, $1, 1, 0, 0, 0 );

        #   2014.01
    }
    elsif ( $date =~ /^([0-9]{4})(?:\.|\-)([0-9]{1,2})$/ ) {
        return mkdate( $1, $2, 1, 0, 0, 0 );

        #   2014.01.21
    }
    elsif ( $date =~ /^([0-9]{4})(?:\.|\-)([0-9]{1,2})$/ ) {
        return mkdate( $1, $2, 1, 0, 0, 0 );
    }
}

1;
